import re
import numpy as np

DATA_FOLDER = 'data/'
TEXT_FOLDER = DATA_FOLDER + 'txt/'
TEXT_FILE = DATA_FOLDER + 'sda.txt'
DATASET_FILE = DATA_FOLDER + 'dataset.npy'


file = open(TEXT_FILE, 'r')
lines = file.readlines()
file.close()

print('Loaded ' + str(len(lines)) + ' from ' + TEXT_FILE)

page_regexp = re.compile('Page [0-9]+ sur [0-9]+')
header_regexp = re.compile('Le seigneur des anneaux')
paragraph_regexp = re.compile(r'.*\.\n')

def extract_paragraphs(lines):
    header = False
    text = []
    for line in lines:
        if header_regexp.match(line) or page_regexp.match(line):
            header = True
        if line == '\n':
            header = False
        else: 
            if not header:
                text.append(line)
    paragraphs = []
    current_paragraph = []

    for line in text:
        current_paragraph.append(line)
        if paragraph_regexp.match(line):
            paragraphs.append(current_paragraph)
            current_paragraph = []

    for i in range(len(paragraphs)):
        paragraphs[i] = " ".join([w.strip() for w in paragraphs[i]]) 
    
    return paragraphs

paragraphs = extract_paragraphs(lines)

dataset = []

for paragraph in paragraphs:
    if(len(paragraph) > 500 and len(paragraph) < 700):
        f = open(TEXT_FOLDER + str(len(dataset)) + ".txt", "w")
        f.write(paragraph)
        dataset.append(paragraph)
        f.close()

np.save(DATASET_FILE, dataset)

print('Successfully created dataset of ' + str(len(dataset)) + ' files.')
