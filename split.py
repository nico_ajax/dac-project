import copy
import argparse
import json

def get_number_of_qa(dataset):
    total_number_of_questions=0
    for element in dataset['data']:
        is_useful=False
        for para in element["paragraphs"]:
            total_number_of_questions+=len(para['qas'])
    return total_number_of_questions

def divide_train_dataset(path, n):
    with open(path, 'r') as f:
        train_dataset = json.load(f)
    result = copy.deepcopy(train_dataset)
    number_of_train_questions = get_number_of_qa(train_dataset)
    while get_number_of_qa(result)>number_of_train_questions/n:
        result['data'].pop()
    return result

def main():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--input_dir",
        default=None,
        type=str,
        required=True,
        help="The input dir.",
    )

    parser.add_argument(
        "--input_file",
        default=None,
        type=str,
        required=True,
        help="The input file to split.",
    )

    parser.add_argument(
        "--divider",
        default=None,
        type=int,
        required=True
    )

    args = parser.parse_args()

    res = divide_train_dataset(args.input_dir + args.input_file, args.divider)

    with open(args.input_dir + args.input_file.split('.')[0] + '_split_{}.json'.format(args.divider), 'w') as f:
        json.dump(res, f)

if __name__ == "__main__":
    main()