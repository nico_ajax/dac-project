# Des Données à la connaissance - Un chatbot pour tous les dominer 

Le but de ce projet est d'appronfondir et appliquer nos connaissances en science de données en les appliquant à un des challenges actuels de l'IA et du NLP qu'est le machine reading comprehension au travers de la mise-en-place d'un système de Question Answering francophone sur un corpus spécifique à une thématique donnée en l'occurence l'univers des Seigneurs des Anneaux de J.R.R Tolkien.

Nous détaillerons dans ce README les principaux éléments de la pipeline que nous avons été amené à mettre en place au niveau de notre code, que ça soit pour l'analyse du dataset ou l'entrainement ou le test du modèle. Nous préciserons également la structure de notre code. Pour tout autre élément, nous vous invitons à consulter les [slides de la présentation finale](https://gitlab.com/nico_ajax/dac-project/-/blob/master/Pr%C3%A9sentations_finales/Presentation_finale.pdf) ainsi que [l'article Medium](https://www.notion.so/Question-Answering-model-on-Lord-of-the-Rings-f10e00d20dc74c25835d99302bc28637) résumant les grandes étapes et conclusions du projet. 

## Approche 

### Constitution et analyse du dataset 

Pour constituer notre dataset, nous avons récupéré une version libre de droit de l'ensemble des textes du Seigneur des Anneaux en version française.  Nous avons alors écrit un script permettant de récupérer l'ensemble du texte et de le segmenter en paragraphes que nous pourrions entrer sur le plateforme d'annnotations mises à disposition par Illuin Technology. Il s'agit du script create_dataset.py qui récupère les paragraphes de taille comprise entre 500 et 700 caractères. On se fixa ensuite un nombre d'annotations à faire par personne pour constituer rapidement un dataset exploitable en essayant comme indiqué de varier la nature des questions et des réponses et reformuler les textes plutôt que de simplement les reprendre. On s'aperçut d'ailleurs que l'aspect narratif du texte plutôt que factuel rendait parfois difficile le format d'annotations imposé par la plateforme (la réponse devant systématiquement être un extrait du texte). 

Une fois le dataset constitué, il s'agissait également de l'analyser un peu plus en détails. Nous avons cherché à faire ressortir les mots les plus utilisés dans notre dataset pour les comparer avec les mots les plus utilisés de la langue française.

Cette analyse a été faite avec le script stats.py. Nous avons récupéré dans un premier temps les mots des scripts du seigneur des anneaux avec les stopwords en moins. Nous avons ensuite récupéré les mots composants les questions de la base de donnée fsquad. Nous avons ensuite créé un score qui fait ressortir les mots les plus courants dans notre corpus mais moins courant dans le dataset de fsquad.

### Entraînements et tests des modèles

Une fois le dataset proprement constitué grâce à la plateforme d'annotations mise à disposition par Illuin Technology, il restait donc à pouvoir utiliser ces annotations pour entraîner différents modèles et les tester. Pour ce faire nous comptions adapter le code de transformers ```run_squad.py``` contenant justement les éléments relatifs à l'entrainement de tels modèles.

A cette fin, la première "brique" dont nous avions besoin était de pouvoir convertir les exports de la plateforme Illuin dans le format pris en entrée par run_squad dont nous avions en exemple le dataset FQuAD conçu par Illuin également sans pour autant que leur plateforme présente un format similaire. Nous avons dans un premier temps réalisé une série de fonction permettant de transformer chaque élément exporté de la plateforme au bon format, de joindre ces différnets éléments en un tout global et finalement de les séparer en un jeu d'entrainement, un jeu de validation et de test afin de pouvoir ensuite procéder à l'entrainement, choisir les hyperparamètres grâce au jeu de validation et obtenir une estimation de la performance de généralisation grâce au jeu de test. 

Une fois cette brique fondamentale, réalisée, nous pouvions passer à l'entrainement des modèles et leur test. Pour ce faire, nous avons réalisés deux scripts à partir du fichier ```run_squad.py```, un permettant de finetuner un modèle donné avec un dataset fourni en entrée sur un certain nombre d'epoch et un permettant d'évaluer un modèle sur des couples de questions/réponses en réalisant des prédictions et en calculant une série de métriques parmi lesquelles on s'intéresserait tout particulièrement au nombre de prédiction exacte et au score F1. 

Nous avons ainsi pu réalisé une série d'expériences afin d'évaluer l'importance de tel ou tel paramètre et aboutir au modèle qui nous semble être le plus performant. pour plus de détails sur ces expériences et leurs résultats, nous vous prions de vous référer aux slides jointes avec le rendu ou à l'article Medium indiqué ci-dessus.

## Structure du code 

Comme expliqué ci-dessus le code ici présent contient du code permettant de formatter les données, les analyser, les séparer en trois dataset (train/valid/test), entraîner des modèles spécifiques sur des données spécifiques et tester les modèles ainsi obtenus. 

Vous trouverez donc : 
- dans le dossier ```Présentation_finales``` le deck de slide récapitulant l'ensemble du projet au format pdf et powerpoint.
- dans le dossier ```data``` les données utilisées pour générer le dataset à annoter et celui-ci
- dans le dossier ```fquad_data``` les datasets de train/valid/valid_short utilisés sur FQuAD
- dans le dossier ```lotr_data``` les dataset issus de notre annotation à utiliser avec les modèles (train, valid, test et train_split_2 contenant la moitié du train dataset)
- dans le dossier ```lotr_raw_data``` les données exportées depuis la plateforme d'annotations par Illuin 
- dans le dossier ```pbs``` les fichiers pbs contenant les scripts à exécuter sur le mésocentre fusion
- dans le dossier ```rendus_intermediaire``` les rendus réalisés au fur et à mesure du cours (à savoir une présentation sur BERT et et un article Medium sur l'active learning )
- dans le dossier ```results``` les résultats de chaque entraînement regroupé par dossier avec les métriques dans un vsc et les prédictions (ainsi que des paramètres) dans différents json
- dans le fichier ```configure``` le script à exécuter pour set up l'environnement sur le mésocentre Fusion 
- dans le fichier ```create_dataset.py``` le code pour créer le dataset à annoter à partir du texte brut
- dans le fichier ```data_processor.py``` le code mettant en forme les exports de la plateforme pour être utilisable par notre code 
- dans le fichier ```freq.json``` l'output de l'analyse du dataset avec les fréquences des différents types de questions/réponses du dataset
- dans le fichier ```predict.py``` le code permettant d'évaluer un modèle entraîné sur un dataset et de calculer les principales métriques 
- dans le fichier ```split.py``` le code permettant de séparer le dataset en train/valid/test
- dans le fichier ```stats.py``` le code permettant d'analyser les propriétés du dataset 
- dans le fichier ```train.py``` le code permettant d'entrainer les modèles 