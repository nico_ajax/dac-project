import os
import torch
import time
import argparse
import logging
from tqdm import tqdm 
import json
import csv

from torch.utils.data import DataLoader, RandomSampler, SequentialSampler

from transformers import (
    CamembertConfig, 
    CamembertForQuestionAnswering, 
    CamembertTokenizer,
    FlaubertConfig,
    FlaubertForQuestionAnsweringSimple,
    FlaubertTokenizer,
    squad_convert_examples_to_features
)

from transformers.data.processors.squad import SquadResult, SquadV2Processor, SquadExample

from transformers.data.metrics.squad_metrics import (
    compute_predictions_logits,
    squad_evaluate
)

MODEL_CLASSES = {
    "camembert": (CamembertConfig, CamembertForQuestionAnswering, CamembertTokenizer),
    "flaubert": (FlaubertConfig, FlaubertForQuestionAnsweringSimple, FlaubertTokenizer),
}

logger = logging.getLogger(__name__)

def createFolder(directory):
    try:
        if not os.path.exists(directory):
            os.makedirs(directory)
    except OSError:
        print ('Error: Creating directory. ' +  directory)


def load_examples(args, tokenizer):
    logger.info("Creating features from dataset file")
    
    processor = SquadV2Processor()

    examples = processor.get_dev_examples(args.data_dir, filename=args.predict_file)

    features, dataset = squad_convert_examples_to_features(
        examples=examples,
        tokenizer=tokenizer,
        max_seq_length=args.max_seq_length,
        doc_stride=args.doc_stride,
        max_query_length=args.max_query_length,
        is_training=False,
        return_dataset="pt",
        threads=args.threads,
    )

    return dataset, examples, features

def run_prediction(args, model, tokenizer, device):
    """Setup function to compute predictions"""
    dataset, examples, features = load_examples(args, tokenizer)

    eval_sampler = SequentialSampler(dataset)
    eval_dataloader = DataLoader(dataset, sampler=eval_sampler, batch_size=10)

    all_results = []


    for batch in tqdm(eval_dataloader, desc="Evaluating"):
        model.eval()
        batch = tuple(t.to(device) for t in batch)

        with torch.no_grad():
            inputs = {
                "input_ids": batch[0],
                "attention_mask": batch[1],
                "token_type_ids": batch[2],
            }

            del inputs["token_type_ids"]

            example_indices = batch[3]

            outputs = model(**inputs)

        for i, example_index in enumerate(example_indices):
            eval_feature = features[example_index.item()]
            unique_id = int(eval_feature.unique_id)

            output = [to_list(output[i]) for output in outputs]

            # Some models (XLNet, XLM) use 5 arguments for their predictions, while the other "simpler"
            # models only use two.
            if len(output) >= 5:
                start_logits = output[0]
                start_top_index = output[1]
                end_logits = output[2]
                end_top_index = output[3]
                cls_logits = output[4]

                result = SquadResult(
                    unique_id,
                    start_logits,
                    end_logits,
                    start_top_index=start_top_index,
                    end_top_index=end_top_index,
                    cls_logits=cls_logits,
                )

            else:
                start_logits, end_logits = output
                result = SquadResult(unique_id, start_logits, end_logits)

            all_results.append(result)

    output_prediction_file = args.output_dir + "predictions.json"
    output_nbest_file = args.output_dir + "nbest_predictions.json"
    output_null_log_odds_file = args.output_dir + "null_predictions.json"

    predictions = compute_predictions_logits(
        examples,
        features,
        all_results,
        args.n_best_size,
        args.max_answer_length,
        args.do_lower_case,
        output_prediction_file,
        output_nbest_file,
        output_null_log_odds_file,
        False,  # verbose_logging
        True,  # version_2_with_negative
        args.null_score_diff_threshold,
        tokenizer,
    )

    results = squad_evaluate(examples, predictions)

    return predictions, results

def save_ordered_dict(ordered_dict, save_dir, file_name):
    keys, values = [], []

    for key, value in ordered_dict.items():
        keys.append(key)
        values.append(value)       

    with open(save_dir + "{}.csv".format(file_name), "w") as outfile:
        csvwriter = csv.writer(outfile)
        csvwriter.writerow(keys)
        csvwriter.writerow(values)

def to_list(tensor):
    return tensor.detach().cpu().tolist()

def main():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--model_type",
        default=None,
        type=str,
        required=True,
        help="Model type selected in the list: " + ", ".join(MODEL_CLASSES.keys()),
    )

    parser.add_argument(
        "--model_name_or_path",
        default=None,
        type=str,
        required=True,
        help="Path to pre-trained model",
    )

    parser.add_argument(
        "--output_dir",
        default=None,
        type=str,
        required=True,
        help="The output directory where the model checkpoints and predictions will be written.",
    )

    parser.add_argument(
        "--data_dir",
        default=None,
        type=str,
        help="The input data dir. Should contain the .json files for the task."
        + "If no data dir or train/predict files are specified, will run with tensorflow_datasets.",
    )

    parser.add_argument(
        "--predict_file",
        default=None,
        type=str,
        help="The input evaluation file. If a data dir is specified, will look for the file there"
        + "If no data dir or train/predict files are specified, will run with tensorflow_datasets.",
    )

    parser.add_argument(
        "--null_score_diff_threshold",
        type=float,
        default=0.0,
        help="If null_score - best_non_null is greater than the threshold predict null.",
    )

    parser.add_argument(
        "--max_seq_length",
        default=384,
        type=int,
        help="The maximum total input sequence length after WordPiece tokenization. Sequences "
        "longer than this will be truncated, and sequences shorter than this will be padded.",
    )
    parser.add_argument(
        "--doc_stride",
        default=128,
        type=int,
        help="When splitting up a long document into chunks, how much stride to take between chunks.",
    )
    parser.add_argument(
        "--max_query_length",
        default=64,
        type=int,
        help="The maximum number of tokens for the question. Questions longer than this will "
        "be truncated to this length.",
    )

    parser.add_argument(
        "--do_lower_case", action="store_true", help="Set this flag if you are using an uncased model."
    )

    parser.add_argument(
        "--n_best_size",
        default=20,
        type=int,
        help="The total number of n-best predictions to generate in the nbest_predictions.json output file.",
    )

    parser.add_argument(
        "--max_answer_length",
        default=30,
        type=int,
        help="The maximum length of an answer that can be generated. This is needed because the start "
        "and end predictions are not conditioned on one another.",
    )

    parser.add_argument("--threads", type=int, default=1, help="multiple threads for converting example to features")

    args = parser.parse_args()
    
    createFolder(args.output_dir)

    # Setup model
    config_class, model_class, tokenizer_class = MODEL_CLASSES[args.model_type]
    config = config_class.from_pretrained(args.model_name_or_path)
    tokenizer = tokenizer_class.from_pretrained(
        args.model_name_or_path, do_lower_case=True)
    model = model_class.from_pretrained(args.model_name_or_path, config=config)

    # "cuda" if torch.cuda.is_available() else 
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    model.to(device)

    processor = SquadV2Processor()

    # Run method
    predictions, results = run_prediction(args, model, tokenizer, device)

    for key in predictions.keys():
        print(predictions[key])
    
    save_ordered_dict(results, args.output_dir, 'metrics')

    return predictions

if __name__ == "__main__":
    main()