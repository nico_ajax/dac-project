Pippin alla à la fenêtre ouest, et son regard tomba sur un étang de brume. La Forêt disparaissait sous le brouillard. C'était comme de regarder d'en dessus une mer de nuages en pente. Il y avait un repli ou un chenal, où la brume se morcelait en maints panaches et vagues: la vallée du Tournesaules. La rivière descendait de la colline sur la gauche pour s'évanouir dans les ombres blanches. A proximité, il y avait un jardin d'agrément avec une haie taillée, couverte d'un réseau d'argent et, au-delà, de l'herbe rase et grise, pâlie par les gouttes de rosée. Aucun saule ne se voyait. .