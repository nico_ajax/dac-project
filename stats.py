import numpy as np
import re
from collections import Counter
from nltk.corpus import stopwords 

DATA_FOLDER = 'data/'
DATASET_FILE = DATA_FOLDER + 'dataset.npy'

stop_words = set(stopwords.words('french'))

def process_paragraphs(paragraphs):
    fullText = " ".join(paragraphs).lower()
    words = re.split(r'(;|,|\s|\'|"|«|»|<|>|\?|-|!|\.|:|’|°)\s*', fullText) 
    filtered_sentence = [w for w in words if not w in stop_words and len(w) > 1]
    return filtered_sentence

dataset = np.load(DATASET_FILE)

counts = Counter(process_paragraphs(dataset))

import json

fquad_contexts = []

with open('data/train.json') as json_file:
    data = json.load(json_file)
    for d in data['data']:
        for p in d["paragraphs"]:
            fquad_contexts.append(p["context"])

fquad_counts = Counter(process_paragraphs(fquad_contexts))

freq_fr = {}

with open('freq.json') as freq_json:
    data = json.load(freq_json)
    for d in data:
        freq_fr[d["label"]] = d["frequency"]

scores = {}

def create_scores(counter, ref):
    scores = {}
    norm_factor = len(ref) / len(counter)
    for word in counter:
        f= 1
        if word in ref:
            f = ref[word]
        scores[word] = counter[word] / f * norm_factor
    return scores

sda_score = create_scores(counts, fquad_counts)
fquad_score = create_scores(fquad_counts, fquad_counts)
    
print(sorted(sda_score, key=sda_score.get, reverse=True)[:100])
#print(sorted(fquad_score, key=fquad_score.get, reverse=True)[:100])
#print(fquad_score["cette"])
#print(sda_score["dit"])
#print(sda_score["frodon"])
#print(counts["frodon"])


#print(fquad_counts.most_common(100))
#print(counts.most_common(100))
#print(freq_fr)
