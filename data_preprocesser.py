from os import listdir, makedirs, remove
from os.path import isfile, join, exists
import shutil
import json
import argparse

def createFolder(directory):
    try:
        if not exists(directory):
            makedirs(directory)
    except OSError:
        print ('Error: Creating directory. ' +  directory)

def emptyFolder(directory):
    for filename in listdir(directory):
        filepath = join(directory, filename)
        try:
            shutil.rmtree(filepath)
        except OSError:
            remove(filepath)
    shutil.rmtree(directory)


def import_data(path, title):
    with open('{path}/annotations/{title}.json'.format(path=path, title=title)) as f:
        data = json.load(f)
    return data

def import_context(path, title):
    with open('{path}/entries/{title}.txt'.format(path=path, title=title), 'r') as f:
        context = f.read()
    return context

def get_output_dict(data, context, title):
    output_dict = {
        'data' : [{
            'title' : title,
            'paragraphs' : [{
                'context' : context,
                'qas' : [] # will be completed
                }]
            }]
        }
    id = 0
    questions = {} # stores the questions that have already appeared
    try:
        for q in data['annotations'][0]['value']:
            if q['question'] in questions.keys(): # if the question has already appeared, we just complete with the new answer
                output_dict['data'][0]['paragraphs'][0]['qas'][questions['questions']]['answer'].append({
                            'answer_start' : q['answer_start'],
                            'text' : q['answer']
                        })
            else: # if the question appears for the first time, we add it both to the ouput dict and to the questions dict
                output_dict['data'][0]['paragraphs'][0]['qas'].append({
                    'answer' : [
                        {
                            'answer_start' : q['answer_start'],
                            'text' : q['answer']
                        }
                    ],
                    'question' : q['question'],
                    'id': str(id).zfill(5) # the id are string corresponding to int starting from 0 and made of 5 digits
                })
                questions[q['question']] = id
                id += 1
    except:
        pass
        # No annotation
    return output_dict

def save_output_json(path, output_dict, title):
    with open('{path}/temp/{title}.json'.format(path=path, title=title), 'w+') as f:
        f.write(json.dumps(output_dict))

def merge_export(folder_with_all_json, only_useful=False):
    mypath = folder_with_all_json
    onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f)) and not f.startswith('.')]
    if only_useful:
        useful_json=[]
        for file in onlyfiles:
            with open(mypath+file, 'r') as f:
                file_dic = json.load(f)
                is_useful=False
                for element in file_dic['data']:
                    for para in element['paragraphs']:
                        if len(para['qas'])>0:
                            is_useful=True
                if is_useful:
                    useful_json.append(file_dic)
    else :
        useful_json=[]
        for file in onlyfiles:
            with open(mypath+file, 'r') as f:
                file_dic = json.load(f)
                useful_json.append(file_dic)
    result={
        'data':[],
        'version':1.0
    }
    id_count= 0
    for file in useful_json:
        for i, element in enumerate(file['data'], start=0):
            for j, para in enumerate(element['paragraphs'], start=0):
                for k, qa in enumerate(para['qas'],start=0):
                    file['data'][i]['paragraphs'][j]['qas'][k]['id']=str(id_count)
                    id_count+=1
        result['data'].extend(file['data'])
    return result

def get_number_of_qa(dataset):
    total_number_of_questions=0
    for element in dataset['data']:
        is_useful=False
        for para in element["paragraphs"]:
            total_number_of_questions+=len(para['qas'])
    return total_number_of_questions

def correct_the_json(json_file):
    # because the annotation platform missed a single s
    for item in json_file['data']:
        for paragraph in item['paragraphs']:
            for question_answering in paragraph['qas']:
                question_answering['answers'] = question_answering.pop('answer')
    return json_file

def train_test_val_split(dataset, train_ratio=0.7, val_ratio=0.2):
    train={
        'data':[],
        'version':1.0
    }
    valid={
        'data':[],
        'version':1.0
    }
    total_number_of_questions=0
    for element in dataset['data']:
        is_useful=False
        for para in element["paragraphs"]:
            total_number_of_questions+=len(para['qas'])
            if len(para['qas'])>0:
                is_useful=True
        if not is_useful:
            train['data'].append(element)
            dataset['data'] = list(filter(lambda i: i['title'] != element['title'], dataset['data'])) 
    number_of_train_questions=int(train_ratio*total_number_of_questions)
    number_of_valid_questions=int(val_ratio*total_number_of_questions)
    while get_number_of_qa(train)<=number_of_train_questions:
        train['data'].append(dataset['data'].pop())
    while get_number_of_qa(valid)<=number_of_valid_questions:
        valid['data'].append(dataset['data'].pop())
    return train, valid, dataset

def main():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--input_dir",
        default=None,
        type=str,
        required=True,
        help="The input directory where the json files are stored.",
    )

    parser.add_argument(
        "--output_dir",
        default=None,
        type=str,
        required=True,
        help="The output directory where the train, valid and test json files will be stored.",
    )

    parser.add_argument(
        "--train_ratio",
        type=float,
        default=0.7,
        help="Ratio of training data.",
    )

    parser.add_argument(
        "--val_ratio",
        type=float,
        default=0.1,
        help="Ratio of validation data.",
    )

    args = parser.parse_args()

    createFolder(args.output_dir)

    for f in listdir("{path}/entries".format(path=args.input_dir)):
        title = f.split('.txt')[0]
        data = import_data(args.input_dir, title)
        context = import_context(args.input_dir, title)
        output_dict = get_output_dict(data, context, title)
        createFolder('{path}/temp/'.format(path=args.output_dir))
        save_output_json(args.output_dir, output_dict, title)


    train, valid, test = train_test_val_split(merge_export('{path}/temp/'.format(path=args.output_dir)), train_ratio=args.train_ratio, val_ratio=args.val_ratio)
    
    train = correct_the_json(train)
    valid = correct_the_json(valid)
    test = correct_the_json(test)

    with open(args.output_dir + 'train.json', 'w') as f:
        json.dump(train, f)
    with open(args.output_dir + 'valid.json', 'w') as f:
        json.dump(valid, f)
    with open(args.output_dir + 'test.json', 'w') as f:
        json.dump(test, f)

    print(get_number_of_qa(train))
    print(get_number_of_qa(valid))
    print(get_number_of_qa(test))

    emptyFolder('{path}/temp/'.format(path=args.output_dir))

if __name__ == "__main__":
    main()